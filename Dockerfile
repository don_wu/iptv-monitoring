FROM python:2.7-alpine
ENV APP_HOME /app
RUN mkdir -p $APP_HOME
RUN apk update
RUN apk add ffmpeg 
RUN apk add ssmtp
WORKDIR $APP_HOME
RUN pip install virtualenv && virtualenv venv && source venv/bin/activate
ADD requirements.txt $APP_HOME/.
RUN pip install -r requirements.txt
ADD iptv-monitoring.py $APP_HOME/.
CMD ["./iptv-monitoring.py"]
