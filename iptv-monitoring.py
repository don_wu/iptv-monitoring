#! /usr/bin/env python2

import ffmpeg
import sys
import os
import signal
import smtplib
from email.mime.text import MIMEText

import ConfigParser
import shutil
import json
import io
import datetime

config = ConfigParser.ConfigParser()
config.read('config.cfg')

#stream_url = config.get('StreamSection', 'stream_url')
stream_url = config.get('StreamSection', 'stream_url', raw=True)
print stream_url
mesg = config.get('MailSection', 'msg')
subject = config.get('MailSection', 'subject')
to = config.get('MailSection', 'to')
sender = config.get('MailSection', 'from')
mesg_ok = config.get('MailSection', 'msg_ok')
subject_ok = config.get('MailSection', 'subject_ok')


# json fuer herrn auer

def json_stuff(zustand=False):

    data = {
        "status": zustand,
        "timestamp": str(datetime.datetime.now())
           }
           
    #with io.open('iptv.json', 'w') as write_file:
    #with io.open('iptv.json', 'w', encoding='utf-8') as write_file:
    with io.open('/usr/share/nginx/html/iptv.json', 'w', encoding='utf-8') as write_file:
        #json.dump(unicode(data), write_file)
        write_file.write(unicode(json.dumps(data, ensure_ascii=False)))


# falls ein erfolgreicher stream reinkam, der sendfolder aber noch existiert -> weg damit

def sendcount_delete():
    if os.path.exists('sendcount'):
       shutil.rmtree('sendcount')
       mail_notification_ok()
# 2 mail notifications are enough
# bei jedem erfolglosen stream kommt ordner dazu 

def add_to_sendfolder():
    if not os.path.exists('sendcount'):
        os.makedirs('sendcount')
    if not os.path.exists('sendcount/countfile1.txt'):
        os.makedirs('sendcount/countfile1.txt')
    elif not os.path.exists('sendcount/countfile2.txt') and os.path.exists('sendcount/countfile1.txt'):
        os.makedirs('sendcount/countfile2.txt')
    
	

def check_sendfolder():
    if os.path.exists('sendcount/countfile2.txt') and os.path.exists('sendcount/countfile1.txt'):
       return True

def mail_notification():
    # did we send twice already
    check = check_sendfolder()
    if check is not True:
       # we did not send twice already
       #msg= "No Multicast Stream in SN 2, testing ORF 1 at udp://239.6.128.5:4000"
       msg = MIMEText(mesg)

       msg['Subject'] = subject
       msg['From'] = sender
       msg['To'] = to

       s = smtplib.SMTP('localhost')
       s.sendmail(msg['From'],msg['To'],msg.as_string())
       s.quit()
       add_to_sendfolder()

def mail_notification_ok():
    msg = MIMEText(mesg_ok)

    msg['Subject'] = subject_ok
    msg['From'] = sender
    msg['To'] = to

    s = smtplib.SMTP('localhost')
    s.sendmail(msg['From'],msg['To'],msg.as_string())
    s.quit()

# Register handler for timeout
def handler(signum, frame):
    print "Forever is over!"
    #raise Exception("end of time")
    #if sys.getsizeof('dummy2.mp4')!= None:
    #if os.path.exists(os.path.abspath('dummy2.mp4')) and os.path.getsize(os.path.abspath('dummy2.mp4')) > 10485750:
    if os.path.exists(os.path.abspath('dummy2.mp4')) and os.path.getsize(os.path.abspath('dummy2.mp4')) > 104:
        #os.system("pkill -15 ffmpeg")
        	print "Alles Leiwand"
		sendcount_delete()
                json_stuff(True)
    else:
        print "Kein Stream"
        # send mail
        json_stuff(False)
        mail_notification()
    os.system("pkill -15 ffmpeg")
    raise Exception("end of time")

def stream_zeugs():
    # testing ORF 1
    
    #stream = "udp://@239.6.128.5:4000"
    stream = stream_url
    stream = ffmpeg.input(stream)
    #stream = ffmpeg.input('http://kojak.wu.ac.at:8075/channel.mpg')
    stream = ffmpeg.filter(stream, 'fps', fps=25, round='up')
    stream = ffmpeg.output(stream, 'dummy2.mp4')

    ffmpeg.run(stream)

# this *may* run forever
def loop_forever():
    import time
    while 1:
        stream_zeugs()

# if dummy file exists, delete
if os.path.exists('dummy2.mp4'):
    os.remove('dummy2.mp4')



# Register the signal function handler
signal.signal(signal.SIGALRM, handler)

signal.alarm(10)

try:
    loop_forever()
except Exception, exc:
    os.system("pkill -15 ffmpeg")
    #print("Alles Leiwand")
    print exc

